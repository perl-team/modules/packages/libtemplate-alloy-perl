libtemplate-alloy-perl (1.022-3) unstable; urgency=medium

  * Remove generated README via debian/clean. (Closes: #1047960)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 19:16:48 +0100

libtemplate-alloy-perl (1.022-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtemplate-alloy-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:30:18 +0100

libtemplate-alloy-perl (1.022-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Lucas Kanashiro ]
  * Add comma in Copyright field in d/copyright
  * Update debian directory copyright

  [ gregor herrmann ]
  * Import upstream version 1.022.
  * Bump debhelper-compat to 13.
  * Set Rules-Requires-Root: no.
  * Add /me to Uploaders.
  * Remove Chris Butler from Uploaders. Thanks for your work!
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Update years of packaging copyright.
  * debian/watch: use uscan version 4.

  [ lintian-brush ]
  * Update standards version to 4.6.0, no changes needed.

  [ Nick Morrott ]
  * d/copyright:
    - Refresh years of Debian copyright
  * d/patches:
    - Add patch skip-stderr-test
    - Add patch typo-in-manual-page
  * d/u/metadata:
    - Restore file and add Bug-* details

 -- Nick Morrott <nickm@debian.org>  Thu, 07 Apr 2022 02:42:57 +0100

libtemplate-alloy-perl (1.020-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 02:41:43 +0100

libtemplate-alloy-perl (1.020-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Update years of packaging copyright.
  * Drop pod-spelling.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Oct 2013 22:12:51 +0200

libtemplate-alloy-perl (1.018-1) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Imported Upstream version 1.018
    + Fixes test failures with Perl 5.18 (Closes: #711258)
    + remove pod.patch, problem fixed upstream
  * Update upstream author email and copyright years
  * add pod-spelling.patch, fixing spelling errors in documentation
  * Standards-Version: 3.9.4 (no changes necessary)
  * use debhelper 9
  * drop trailing slash from upstream URL
  * simplyfy .examples file
  * bring debian/copyright to copyright-format 1.0 spec

 -- Damyan Ivanov <dmn@debian.org>  Fri, 23 Aug 2013 10:57:36 +0300

libtemplate-alloy-perl (1.016-1) unstable; urgency=low

  [ gregor herrmann ]
  * Convert to source format 3.0 (quilt).
  * Add a patch to fix two POD errors.

  [ Chris Butler ]
  * New upstream release
  * Refreshed pod.patch
  * Add myself to Uploaders and copyright
  * Update copyright file to latest DEP-5 format
  * Remove the version from the perl build-dependency, as per new perl policy
  * Use short dh7 rules file
  * Upped Standards-Version to 3.9.1 (no changes required)

 -- Chris Butler <chrisb@debian.org>  Sun, 27 Feb 2011 23:40:42 +0000

libtemplate-alloy-perl (1.013-3) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Christoph Berg ]
  * Remove myself from Uploaders.

 -- Christoph Berg <myon@debian.org>  Thu, 10 Dec 2009 12:28:15 +0100

libtemplate-alloy-perl (1.013-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: improve long description, thanks to Gerfried Fuchs for the
    bug report (closes: #527037).
  * Don't install README anymore (text version of the POD).
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}.
  * debian/copyright: switch to new format and add information about
    additional copyright holder.

  [ Christoph Berg ]
  * debian/libtemplate-alloy-perl.examples: Avoid installing .svn directory.
  * Add acknowledgement note to debian/copyright.

 -- Christoph Berg <myon@debian.org>  Tue, 26 May 2009 13:49:36 +0200

libtemplate-alloy-perl (1.013-1) unstable; urgency=low

  * Initial Release.

 -- Christoph Berg <myon@debian.org>  Wed, 15 Apr 2009 11:23:42 +0200
